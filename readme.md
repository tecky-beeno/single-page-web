# single-page-web

This repo demo how to make interactive webapp within 10 lines of code.

You can dynamically switch between pages without reloading the whole page using `fetch()` on html files.

Example see [index.html](./public/index.html)

Bonus: also checkout [multi-page-web](../../../../multi-page-web) for greater flexibility on the layout

## Get Started

You can run a web server with node.js:
```bash
npx http-server
```

or python 3:
```bash
python -m http.server --directory public
```

or python 2:
```bash
cd public
python -m SimpleHTTPServer
```
